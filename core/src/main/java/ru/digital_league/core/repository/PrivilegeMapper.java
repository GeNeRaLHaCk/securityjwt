package ru.digital_league.core.repository;

import jdk.nashorn.internal.runtime.options.Option;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface PrivilegeMapper <PrivilegeEntity, Long> {
    Option<PrivilegeEntity> findPrivilegeByName(String name);
}
