package ru.digital_league.core.repository;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import ru.digital_league.core.entity.PersonEntity;

@Mapper
@Repository
public interface PersonMapper {
    int deleteByPrimaryKey(Long personId);

    int insert(PersonEntity record);

    int insertSelective(PersonEntity record);

    PersonEntity selectByPrimaryKey(Long personId);

    int updateByPrimaryKeySelective(PersonEntity record);

    int updateByPrimaryKey(PersonEntity record);

}
