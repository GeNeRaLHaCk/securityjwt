package ru.digital_league.core.repository;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import ru.digital_league.core.entity.EmploymentEntity;

import java.util.List;
import java.util.Set;

@Repository
@Mapper
public interface EmploymentMapper {
    int deleteByPrimaryKey(Long employmentId);

    int insert(EmploymentEntity record);

    int insertSelective(EmploymentEntity record);

    EmploymentEntity selectByPrimaryKey(Long employmentId);

    int updateByPrimaryKeySelective(EmploymentEntity record);

    int updateByPrimaryKey(EmploymentEntity record);

    List<EmploymentEntity> getByPersonId(Long personId);

    void insertAll(@Param("list") List<EmploymentEntity> employmentsToSave);

    void updateAll(@Param("list") List<EmploymentEntity> employmentsToUpdate);

    void deleteByIds(@Param("ids") Set<Long> ids);

}
