package ru.digital_league.core.repository;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import ru.digital_league.core.entity.UserEntity;

import java.util.List;

@Mapper
@Repository
public interface UsersrolesMapper {
/*    int deleteSelective(@Param("userid") Long userid,
                        @Param("roleid") Long roleid);*/
    int insertSelective(@Param("userid") Long userid,
                        @Param("roleid") Long roleid);

    List<UserEntity> findAllByRole(@Param("role") Long role);
/*
    List<RoleEntity> findAllByUser(@Param("user") Long user);*/
}