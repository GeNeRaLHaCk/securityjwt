package ru.digital_league.core.repository;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import ru.digital_league.core.entity.RoleEntity;

@Repository
@Mapper
public interface RoleMapper{
    RoleEntity findRoleEntityByName(String name);
}
