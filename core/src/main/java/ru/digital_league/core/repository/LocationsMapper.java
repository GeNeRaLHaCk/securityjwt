package ru.digital_league.core.repository;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import ru.digital_league.core.entity.LocationsEntity;

import java.util.List;
import java.util.Optional;

@Mapper
public interface LocationsMapper {
    int deleteByPrimaryKey(Integer locationId);

    int insert(LocationsEntity record);

    LocationsEntity selectByPrimaryKey(Integer locationId);

    int updateByPrimaryKeySelective(LocationsEntity record);

    Optional<LocationsEntity> getByLocationId(@Param("locationId") Integer locationId);

    List<LocationsEntity> selectAll();

    int updateBatch(List<LocationsEntity> list);
}