package ru.digital_league.core.repository;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import ru.digital_league.core.entity.PrivilegeEntity;
import ru.digital_league.core.entity.RoleEntity;

import java.util.List;

@Mapper
@Repository
public interface RolesprivilegesMapper {
    List<RoleEntity> findAllByPrivilegeid(
            @Param("privilegeid") Long privilegeid);
    List<PrivilegeEntity> findAllByRoleid(
            @Param("roleid") Long roleid);
    int insertSelective(@Param("roleid") Long roleid,
                        @Param("privilegeid") Long privilegeid);
    int deleteSelective(@Param("roleid") Long roleid,
                        @Param("privilegeid") Long privilegeid);
}