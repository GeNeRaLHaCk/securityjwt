package ru.digital_league.core.config;

import lombok.val;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import ru.digital_league.core.entity.LocationsEntity;

import javax.jms.ConnectionFactory;
import java.util.Collections;

@Configuration
@EnableAutoConfiguration
@EnableJms
//@ImportResource("classpath:activemq.xml")
public class ActiveMQConfig {
    //реализуем конвертер т.к. по умолчанию умеет мало
    @Bean
    public MappingJackson2MessageConverter messageConverter(){
        //переприсвоить нельзя
        val messageConverter = new MappingJackson2MessageConverter();
        messageConverter.setTypeIdPropertyName("content-type");
        messageConverter.setTypeIdMappings(
                Collections.singletonMap(
                        "locationsEntity", LocationsEntity.class));
       return messageConverter;
    }
    @Bean
    public DefaultJmsListenerContainerFactory
            jmsListenerContainerFactory(
            @Qualifier("jmsConnectionFactory") ConnectionFactory connectionFactory){
        DefaultJmsListenerContainerFactory
                jmsListenerContainerFactory =
                new DefaultJmsListenerContainerFactory();
        jmsListenerContainerFactory.setConnectionFactory(
                connectionFactory);
        jmsListenerContainerFactory.setMessageConverter(messageConverter());
        jmsListenerContainerFactory.setConcurrency("5-10");
        return jmsListenerContainerFactory;
    }
}
