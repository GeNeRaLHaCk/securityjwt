package ru.digital_league.core.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.digital_league.core.service.UserService;

@EnableWebSecurity(debug = true)
//@EnableConfigurationProperties
@ComponentScan("ru.digital_league.core")
//@EnableGlobalMethodSecurity(securedEnabled = true) //это в случае если всё
// таки пробрались через защиту, чтобы защитить МЕТОДЫ
// обозначим их аннотацией @Secured("Имя роли, права")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
//websecurconfigadapter позволяет настрить всю систему security под свои нужды
//полностью отключаем автоконфигурацию приложения по умолчанию
    @Autowired
    private UserService userService;

    @Bean
    public BCryptPasswordEncoder encoder (){
        return new BCryptPasswordEncoder();
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean()
            throws Exception {
        return super.authenticationManagerBean();
    }

    @Autowired
    protected void configureAuthentication(AuthenticationManagerBuilder builder,
                                           BCryptPasswordEncoder encoder)
            throws Exception {
        System.out.println("AUTHMANAGERBUILDER");
        builder.userDetailsService(this.userService)
                .passwordEncoder(encoder);
    }

    @Override
    protected void configure(HttpSecurity httpSecurity)
            throws Exception {
        httpSecurity
                .csrf().disable()//timeleaf по умолчанию включает это, от инжекций
                .formLogin().disable()
                .httpBasic().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,
                        "/",
                        "/v3/**",
                        "/*.html",
                        "/favicon.ico",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js"
                ).permitAll()
                .antMatchers("/login/**").permitAll()
                .antMatchers("/register/**").permitAll()
                .antMatchers("/users/**").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .logout();
    }

 /*   @Bean
    public JdbcUserDetailsManager users(DataSource dataSource){
        //создать базовых юзеров сразу
        UserDetails user = User.builder()
                .username("user")
                .password("{bcrypt}")
                .roles("USER")
                .build();
        UserDetails admin = User.builder()
                .username("user")
                .password("{bcrypt}")
                .roles("ADMIN")
                .build();
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager(dataSource);
        if(jdbcUserDetailsManager.userExists(user.getUsername()))
            jdbcUserDetailsManager.deleteUser(user.getUsername());
        if(jdbcUserDetailsManager.userExists(admin.getUsername()))
            jdbcUserDetailsManager.deleteUser(admin.getUsername());
        jdbcUserDetailsManager.createUser(user);
        jdbcUserDetailsManager.createUser(admin);
        return jdbcUserDetailsManager;
    }*/

/*    @Bean
    public DaoAuthenticationProvider daoAuthenticationProvider(){
        //отдаём ему логин пароль, а он должен сказать существует такой юзер или нет
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setPasswordEncoder(encoder());
        authenticationProvider.setUserDetailsService(userService);
        return authenticationProvider;
    }*/
/*    @Bean
    public FilterRegistrationBean disableAutoRegistration(TokenAuthenticationFilter filter) {
        FilterRegistrationBean registration = new FilterRegistrationBean(filter);
        registration.setEnabled(false);//в противном случае
        // фильтр регистрируется дважды(в конфигурации и
        // путем автоматического обнаружения
        return registration;
    }*/
}