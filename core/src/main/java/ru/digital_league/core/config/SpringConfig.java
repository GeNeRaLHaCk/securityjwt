package ru.digital_league.core.config;

import org.modelmapper.ModelMapper;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@Configuration
@EnableAutoConfiguration
@ComponentScan("ru.digital_league")
@ComponentScan("ru.digital_league.core.service")
@ComponentScan("ru.digital_league.core.controllers")
@EnableWebMvc
@PropertySource("classpath:application.yml")
@PropertySource("classpath:application.properties")
@EnableJms
public class SpringConfig {
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
