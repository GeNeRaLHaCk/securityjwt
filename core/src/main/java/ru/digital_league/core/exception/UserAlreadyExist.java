package ru.digital_league.core.exception;

public class UserAlreadyExist extends Exception {
    public UserAlreadyExist(String message){
        super(message);
    }
}
