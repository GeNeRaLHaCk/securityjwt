package ru.digital_league.core.activemq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;
import ru.digital_league.core.entity.LocationsEntity;

@Component
@Slf4j
public class ActiveMQConsumer {
    @JmsListener(destination = "queue")
    @SendTo("queue-answer")
    public String receiveMessage(LocationsEntity locationsEntity){
        log.info("Receive message:" + locationsEntity);
        return "Yeeees";
    }
}
