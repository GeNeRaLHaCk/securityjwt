package ru.digital_league.core.security;

import org.springframework.security.web.RedirectStrategy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class NoRedirectStrategy implements RedirectStrategy {
    public NoRedirectStrategy(){}
    @Override
    public void sendRedirect(HttpServletRequest httpServletRequest,
                             HttpServletResponse httpServletResponse,
                             String s) throws IOException {
        httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "NONONON");
    }
}
