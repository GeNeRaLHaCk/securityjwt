/*
package ru.digital_league.core.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
<<<<<<< HEAD
import ru.digital_league.core.entity.PrivilegeEntity;
import ru.digital_league.core.entity.RoleEntity;
import ru.digital_league.core.entity.UserEntity;
import ru.digital_league.core.repository.PrivilegeMapper;
import ru.digital_league.core.repository.RoleMapper;
import ru.digital_league.core.repository.UserRepository;
=======
import ru.digital_league.domain.entity.PrivilegeEntity;
import ru.digital_league.domain.entity.RoleEntity;
import ru.digital_league.domain.entity.UserEntity;
import ru.digital_league.domain.repository.PrivilegeMapper;
import ru.digital_league.domain.repository.RoleMapper;
import ru.digital_league.domain.repository.UserRepository;
>>>>>>> 810d52e5f3def3c9d502227a4e062cdf58d03c43

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public class SetupDataLoader implements ApplicationListener
        <ContextRefreshedEvent> {
    boolean alreadySetup = false;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleMapper roleRepository;

    @Autowired
    private PrivilegeMapper privilegeRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        if (alreadySetup)
            return;
        PrivilegeEntity readPrivilege
                = createPrivilegeIfNotFound("READ_PRIVILEGE");
        PrivilegeEntity writePrivilege
                = createPrivilegeIfNotFound("WRITE_PRIVILEGE");

        List<PrivilegeEntity> adminPrivileges = Arrays.asList(
                readPrivilege, writePrivilege);
        createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
        createRoleIfNotFound("ROLE_USER",
                Arrays.asList(readPrivilege));

        RoleEntity adminRole = roleRepository.
                findRoleEntityByName("ROLE_ADMIN");
        UserEntity user = new UserEntity();
        user.setUsername("test");
        user.setPassword(passwordEncoder.encode("123"));
        user.setEnable(true);
        user.setRoles(user, (Set<RoleEntity>) Arrays.asList(adminRole));
        userRepository.save(user);

        alreadySetup = true;
    }

    @Transactional
    PrivilegeEntity createPrivilegeIfNotFound(String name) {
        PrivilegeEntity privilege = privilegeRepository.findPrivilegeByName(name);
        if (privilege == null) {
            privilege = new PrivilegeEntity(name);
            privilegeRepository.save(privilege);
        }
        return privilege;
    }

    @Transactional
    RoleEntity createRoleIfNotFound(
            String name, Collection<PrivilegeEntity> privileges) {

        RoleEntity role = roleRepository.findRoleEntityByName(name);
        if (role == null) {
            role = new RoleEntity(name);
            role.setPrivileges(privileges);
            roleRepository.save(role);
        }
        return role;
    }
}
*/
