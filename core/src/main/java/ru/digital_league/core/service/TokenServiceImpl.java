/*
package ru.digital_league.core.service;

import org.springframework.security.core.token.Token;
import org.springframework.security.core.token.TokenService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.digital_league.core.entity.UserEntity;
import ru.digital_league.core.model.RefreshToken;
import ru.digital_league.core.model.TokenProperties;
import ru.digital_league.core.utils.JwtUtils;

import java.util.Date;

import static java.time.Instant.now;
import static ru.digital_league.core.utils.JwtUtils.buildToken;
import static ru.digital_league.core.utils.JwtUtils.parseToken;
public class TokenServiceImpl implements TokenService {

    private final TokenStore tokenStore;

    private final UserService userService;

    private final TokenProperties tokenProperties;

    public TokenServiceImpl(
            TokenStore tokenStore,
            UserService userService,
            TokenProperties tokenProperties) {
        this.tokenStore = tokenStore;
        this.userService = userService;
        this.tokenProperties = tokenProperties;
    }

    @Override
    public String getUsername(String token) {
        return parseToken(token, tokenProperties.getSecret()).
                getSubject();
    }

    @Override
    public String newAccessToken(UserContext ctx) {
        return buildToken(JwtUtils.TokenData.builder()
        .subject(ctx.getLogin())
        .secret(tokenProperties.getSecret())
        .validityMillis(tokenProperties.getAccessTokenValidityMillis())
        .build());
    }

    @Override
    public boolean isValidAccessToken(String token) {
        try{
            parseToken(token,tokenProperties.getSecret());
            return true;
        } catch(Exception e){
            return false;
        }
    }

    @Override
    public RefreshToken newRefreshToken(UserContext ctx) {
        UserEntity user = null;
        try {
            user = (UserEntity) userService.loadUserByUsername(
                    ctx.getLogin());
        } catch (UsernameNotFoundException e){
            throw new UsernameNotFoundException("Username" +
                    "with that name is not found");
        }
        if(user == null) return null;
        RefreshToken token = new RefreshToken();
        token.setUser(user);
        token.setId(user.getId());
        token.setExpiryTime(new Date(System.currentTimeMillis()
        + tokenProperties.getRefreshTokenValidityMillis()));
        return tokenStore.store(token);
    }

    @Override
    public RefreshToken findRefreshToken(Long tokenId) {
        return tokenStore.find(tokenId);
    }

    @Override
    public boolean isValidRefreshToken(RefreshToken token) {
        return !now().isAfter(token.getExpiryTime().toInstant());
    }

    @Override
    public Token allocateToken(String s) {
        return null;
    }

    @Override
    public Token verifyToken(String s) {
        return null;
    }
}
*/
