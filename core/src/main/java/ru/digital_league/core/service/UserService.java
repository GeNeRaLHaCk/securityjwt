package ru.digital_league.core.service;

import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.digital_league.core.entity.PrivilegeEntity;
import ru.digital_league.core.entity.RoleEntity;
import ru.digital_league.core.entity.UserEntity;
import ru.digital_league.core.repository.RolesprivilegesMapper;
import ru.digital_league.core.repository.UsersrolesMapper;
import ru.digital_league.core.repository.UsertableMapper;

import java.util.List;

@Service
public class UserService implements UserDetailsService {

    private final UsertableMapper usertableMapper;

    private final RolesprivilegesMapper rolesprivilegesMapper;

    private final UsersrolesMapper usersrolesMapper;
    @Autowired
    public UserService(RolesprivilegesMapper rolesprivilegesMapper,
                       UsersrolesMapper usersrolesMapper,
                       UsertableMapper usertableMapper, BCryptPasswordEncoder encoder) {
        this.rolesprivilegesMapper = rolesprivilegesMapper;
        this.usersrolesMapper = usersrolesMapper;
        this.usertableMapper = usertableMapper;
    }

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        UserEntity user = usertableMapper.selectByUsername(username);
        System.out.println("DDDDDDDDDDDDDDdD "  + user );
        if(user == null)
            throw new UsernameNotFoundException(
                    "User with that login is not found");
        return new User(user.getUsername(),
                user.getPassword(), user.isEnabled(),
                user.isAccountNonExpired(),
                user.isCredentialsNonExpired(),
                user.isAccountNonLocked(),
                user.getAuthorities());
    }

    @Transactional
    public UserEntity findUserByUsernameAndPassword(String username
                                                    , String password){
/*        System.out.println("kfksadkdsf" + username + password);*/
        UserEntity user = usertableMapper.
                selectByUsernameAndPassword(username, password);
        return user;
/*        UserEntity user = userMapper.getUserByUsernameAndPassword(username, password);
        return user;*/
    }
    @Transactional
    public List<RoleEntity> findRoleByPrivilegeId(Long privilegeId){
        List<RoleEntity> role =
                rolesprivilegesMapper.findAllByPrivilegeid(privilegeId);
        return role;
    }
    @Transactional
    public List<PrivilegeEntity> findPrivilegeByRoleId(Long roleId){
        List<PrivilegeEntity> privilege =
                rolesprivilegesMapper.findAllByRoleid(roleId);
        return privilege;
    }
    @Transactional
    public List<UserEntity> findUserByRole(Long roleId){
        List<UserEntity> users =
                usersrolesMapper.findAllByRole(roleId);
        return users;
    }
    @Transactional
    public UserEntity findUserById(Long userId){
/*        Optional<U\serEntity> userFromDb = userRepository.findById(userId);
        return userFromDb.orElse(new UserEntity());*//*
        UserEntity user = userMapper.getUserById(userId);
        return user;*/
        return new UserEntity();
    }

    @Transactional
    public boolean saveUser(UserEntity user) throws PSQLException {
        usertableMapper.insert(user);
        usersrolesMapper.insertSelective(user.getUserId(),
                user.getRoles().get(0).getRoleid());
        System.out.println("VACHEsdfasdfasdf " + user);
        return true;
        //usersrolesMapper.insertSelective()
    }
    @Transactional
    public boolean deleteUser(Long userId){
return true;
    }
/*    public List<UserEntity> usergtList(Long idMin){
        return em.createQuery("Select u from user u where u.id > :paramId"
                ,UserEntity.class).setParameter("paramId", idMin).
                getResultList();
    }*/
}
