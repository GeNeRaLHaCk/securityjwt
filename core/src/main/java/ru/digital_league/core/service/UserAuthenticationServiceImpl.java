package ru.digital_league.core.service;

import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import ru.digital_league.api.UserAuthenticationService;
import ru.digital_league.core.entity.UserEntity;
import ru.digital_league.domain.dto.AccountDTO;
import ru.digital_league.domain.dto.LogInDTO;
import ru.digital_league.domain.dto.RegisterDTO;

@Service
public class UserAuthenticationServiceImpl implements UserAuthenticationService {
    @Autowired
    public UserService userService;
    @Autowired
    public AuthenticationManager authManager;
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public Authentication authorize(@RequestBody @Validated LogInDTO loginDto)
            throws RuntimeException {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(
                        loginDto.getLogin(),
                        loginDto.getPassword());
        Authentication authentication =
                authManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().
                setAuthentication(authentication);
        return authentication;
    }

    //@Secured("ROLE_ADMIN")
    @Override
    public AccountDTO registerUser(RegisterDTO registerDto)
            throws Exception {
        UserEntity userEntity = createUser(registerDto);
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setUserid(userEntity.getUserId());
        accountDTO.setUsername(userEntity.getUsername());
        accountDTO.setPassword(userEntity.getPassword());
        accountDTO.setEnabled(userEntity.isEnabled());
        return accountDTO;
    }

    private UserEntity createUser(RegisterDTO registerDto)
            throws PSQLException {
        UserEntity user = new UserEntity();
        user.setUsername(registerDto.getLogin());
        user.setPassword(passwordEncoder.encode(registerDto.getPassword()));
        user.setRoleUser();
        userService.saveUser(user);
        return user;
    }
    @Override
    public void logout() {
        SecurityContextHolder.clearContext();
    }

/*    @Override
    public Optional<UserEntity> findByToken(String token) {
        UserEntity userEntity = userService.
                findUserByUsernameAndPassword("test", "123");
*//*        UserEntity user = new UserEntity();
        usertable.setUserid(usertable.getUserid());
        usertable.setUsername(usertable.getUsername());
        usertable.setPassword(usertable.getPassword());*//*
        return Optional.of(userEntity);
        //userService.findToken(token);
    }*/
}
