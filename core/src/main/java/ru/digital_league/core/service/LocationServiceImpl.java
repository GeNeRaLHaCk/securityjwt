package ru.digital_league.core.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.digital_league.api.LocationsService;
import ru.digital_league.core.entity.LocationsEntity;
import ru.digital_league.core.repository.LocationsMapper;
import ru.digital_league.domain.dto.LocationsDTO;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class LocationServiceImpl implements LocationsService {
    private final LocationsMapper locationsMapper;
    private final ModelMapper modelMapper;

    @Override
    @Transactional(readOnly = true)
    public LocationsDTO getLocationById(Integer id) {
        LocationsEntity locationsEntity = locationsMapper.getByLocationId(id).orElseThrow(
                () -> new RuntimeException(
                        String.format(
                                "Не найдена сущность локации по идентификатору=%s", id)
                )
        );
        log.debug("Получена сущность с id={}", id);
        return modelMapper.map(locationsEntity, LocationsDTO.class);
    }

    @Override
    @Transactional
    public LocationsDTO insertLocation(LocationsDTO dto) {
        LocationsEntity locationsEntity = modelMapper.map(dto, LocationsEntity.class);
        locationsMapper.insert(locationsEntity);
        log.info("Создана сущность с id={}", locationsEntity.getLocationId());
        return getLocationById(locationsEntity.getLocationId());
    }

    @Override
    @Transactional(readOnly = true)
    public List<LocationsDTO> getAll() {
        List<LocationsEntity> entities = locationsMapper.selectAll();
        log.debug("Получены все сущности");
        return modelMapper.map(entities, TypeToken.of(List.class).getType());
    }

}
