package ru.digital_league.core.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.digital_league.api.LocationsService;
import ru.digital_league.domain.dto.LocationsDTO;

import java.util.List;

@RestController
@RequestMapping("/locations")
@RequiredArgsConstructor
@Tag(name = "location", description = "АПИ для локаций")
public class LocationsController {
    private final LocationsService locationsService;

    @GetMapping("/{locationId}")
    @Operation(summary = "Получение локации по идентификатору")
    public LocationsDTO getLocationById(@PathVariable Integer locationId) {
        return locationsService.getLocationById(locationId);
    }

    @PostMapping
    @Operation(summary = "Создание новой локации")
    public LocationsDTO insertLocation(@RequestBody LocationsDTO dto) {
        return locationsService.insertLocation(dto);
    }

    @GetMapping
    @Operation(summary = "Получение всех локаций")
    public List<LocationsDTO> getAll() {
        return locationsService.getAll();
    }

}
