package ru.digital_league.core.controllers;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.digital_league.api.UserAuthenticationService;
import ru.digital_league.core.service.UserService;
import ru.digital_league.domain.dto.AccountDTO;
import ru.digital_league.domain.dto.LogInDTO;
import ru.digital_league.domain.dto.RegisterDTO;

@RestController
@Log
public class AuthController {
    @Autowired
    private UserService userService;
/*    @Autowired
    private JwtUtils jwtProvider;*/
    @Autowired
    @Qualifier("userAuthenticationServiceImpl")
    private UserAuthenticationService authentication;

    @GetMapping("/users")
    public ResponseEntity role(@RequestParam(
            name = "role") Long roleId){
        return ResponseEntity.ok().body(userService.
                findUserByRole(roleId).
                stream().map(user->user));
    }
    @GetMapping("/login/{login}/{password}")
    public Authentication authorize(
	@PathVariable("login") String login,
	@PathVariable("password") String password)
	//@RequestBody
      //                              @Validated
        //                                    LogInDTO logInDto) 
		{
		LogInDTO logInDTO = new LogInDTO();
		logInDTO.setLogin(login);
		logInDTO.setPassword(password);
        Authentication auth = authentication.authorize(logInDTO);
        return auth;
    }

    @PostMapping("/register")
    public ResponseEntity register(@RequestBody
                                       @Validated
                                               RegisterDTO registerDTO){
        try {
            AccountDTO account = authentication.registerUser(registerDTO);
            return ResponseEntity.ok().body(account);
        } catch (Exception e) {
            if(e.getMessage().contains("duplicate key value"))
                return ResponseEntity.ok().body(
                        "User with that name already exist");
            return ResponseEntity.ok().body(e.getMessage());
        }
    }
    @GetMapping("/current")//заинжектить Principal текущий пользователь
    public User getCurrent(@AuthenticationPrincipal User user) {
        System.out.println("CURR: " + user);
        return user;
    }

    @GetMapping("/logout")
    public ResponseEntity logout(@AuthenticationPrincipal User user) {
        System.out.println("Logout: " + user);
        authentication.logout();
        return ResponseEntity.ok().body("Successfull logout");
    }
}
