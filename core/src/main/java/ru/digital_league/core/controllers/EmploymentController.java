/*
package ru.digital_league.core.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.digital_league.domain.api.EmploymentService;
import ru.digital_league.domain.dto.EmploymentDTO;

@RestController
@RequestMapping(name = "/api/employment")
public class EmploymentController {
    @Autowired
    private EmploymentService employmentService;

    @PostMapping("/api/employments/update/{personId}")
    public EmploymentDTO saveAccount(
            @RequestBody
            @Validated EmploymentDTO employmentDTO) {
        return employmentService.save(employmentDTO);
    }
}
*/
