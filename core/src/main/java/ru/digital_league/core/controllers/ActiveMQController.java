package ru.digital_league.core.controllers;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.digital_league.core.entity.LocationsEntity;

@RestController
@RequestMapping("/activemq")
public class ActiveMQController {
    //контроллеры работают в отдельных потоках
    //поскольку у нас все работает на сервлетах
    @Autowired
    private JmsTemplate jmsTemplate;
    @GetMapping(value = "/send/{message}")
    private String sendMessage(@PathVariable("message")
                               String message){
        val locations = new LocationsEntity();
        locations.setLocationId(1);
        locations.setStreetAddress("Baker Street");
        locations.setCity("London");
        jmsTemplate.convertAndSend("queue", locations);
        return "done, sent:" + locations;
    }
}
