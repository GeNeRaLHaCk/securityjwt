/*
package ru.digital_league.core.utils;

import io.jsonwebtoken.*;
import lombok.Builder;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

@Component
public class JwtUtils {
    @Data
    @Builder
    public static class TokenData{
        private String secret = "crazy";
        private String subject;
        private String username;
        private long validityMillis;
    }
    public JwtUtils(){}

    public static String buildToken(TokenData tokenData){
        LocalDateTime timeNow = LocalDateTime.now();
        LocalDateTime exp = timeNow.plusNanos(
                MILLISECONDS.toNanos(tokenData.getValidityMillis()));
        return Jwts.builder()
                .setSubject(tokenData.getSubject())
                .claim("username", tokenData.getUsername())
                .signWith(SignatureAlgorithm.HS512, tokenData.getSecret())
*/
/*                .setIssuedAt(Date.from(timeNow.atZone(
                        ZoneId.systemDefault()).toInstant()))*//*

                .setExpiration(Date.from(exp.atZone( //длительность действия
                        //после того как пройдёт - исключение в методе validateToken
                        ZoneId.systemDefault()).toInstant())).
                compact();
    }

    public static Claims parseToken(String token){
        return Jwts.parser()
                .setSigningKey(TokenData.builder().secret)
                .parseClaimsJws(token)
                .getBody();
    }
    public static boolean validateToken(String token) throws Exception {
        try {
            Jwts.parser().setSigningKey(TokenData.builder().secret).parseClaimsJws(token);
            return true;
        } catch (ExpiredJwtException expEx) {
            throw new Exception("Token expired");
        } catch (UnsupportedJwtException unsEx) {
            throw new Exception("Unsupported jwt");
        } catch (MalformedJwtException mjEx) {
            throw new Exception("Malformed jwt");
        } catch (SignatureException sEx) {
            throw new Exception("Invalid signature");
        } catch (Exception e) {
            throw new Exception("invalid token");
        }
        //return false;
    }
}
*/
