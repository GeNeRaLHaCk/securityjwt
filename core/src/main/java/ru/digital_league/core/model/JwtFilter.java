/*
<<<<<<< HEAD:core/src/main/java/ru/digital_league/core/model/JwtFilter.java
package ru.digital_league.core.model;
=======
package ru.digital_league.domain.model;
>>>>>>> 810d52e5f3def3c9d502227a4e062cdf58d03c43:dto/src/main/java/ru/digital_league/domain/model/JwtFilter.java

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;
<<<<<<< HEAD:core/src/main/java/ru/digital_league/core/model/JwtFilter.java
import ru.digital_league.core.entity.UserEntity;
import ru.digital_league.core.service.UserService;
import ru.digital_league.core.utils.JwtUtils;
=======
import ru.digital_league.domain.entity.UserEntity;
import ru.digital_league.domain.service.UserService;
import ru.digital_league.domain.utils.JwtUtils;
>>>>>>> 810d52e5f3def3c9d502227a4e062cdf58d03c43:dto/src/main/java/ru/digital_league/domain/model/JwtFilter.java

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static org.springframework.util.StringUtils.hasText;
//GenericFilterBean по сути, обычная базовая иплементация javax.servlet.filter
//для фильтрации запросоов на сервлет
@Component
public class JwtFilter extends GenericFilterBean {
    public static final String AUTHORIZATION = "Authorization";
    @Autowired
    private JwtUtils jwtProvider;
    @Autowired
    private UserEntity user;
    @Autowired
    private UserService userService;
    //именно этот метот будет срабатывать при РАБОТЕ ФИЛЬТРА
    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain)
            throws IOException, ServletException {
        logger.info("do filter...");
        String token = getTokenFromRequest((HttpServletRequest) servletRequest);
        try {
            if(token != null && jwtProvider.validateToken(token)){
                String userLogin = jwtProvider.parseToken(token).getSubject();
                UserEntity userDetails = (UserEntity) userService.loadUserByUsername(userLogin);
                UsernamePasswordAuthenticationToken auth =
                        new UsernamePasswordAuthenticationToken(
                                userDetails,
                                null,
                                userDetails.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(auth);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    public String getTokenFromRequest(HttpServletRequest request){
        String bearer = request.getHeader(AUTHORIZATION);
        if(hasText(bearer) && bearer.startsWith("Bearer "))
            return bearer.substring(7);
        return null;
    }
}
*/
