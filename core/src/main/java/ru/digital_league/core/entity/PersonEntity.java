package ru.digital_league.core.entity;

import org.jetbrains.annotations.NotNull;

import java.util.Date;


public class PersonEntity {

    private Long personId;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    private Date birthDate;

    @NotNull
    private String gender;

}
