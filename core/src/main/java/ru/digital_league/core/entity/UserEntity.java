package ru.digital_league.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
@NoArgsConstructor
@AllArgsConstructor
@Data
@Primary
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
//Наш Principal(Личность пользователя)
public class UserEntity implements UserDetails{
    private Long userid;
    private String username;
    private String password;
    private boolean enabled = true;
    private String passwordConfirm;
    private List<RoleEntity> roles = new ArrayList<>();
    @Override
    @JsonIgnore
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoles();
    }
    //одна из имплементаций grantdauthority - simplegrantdauthority
    //в которую можно добавить только имя роли и Spring будет давать доступ если
    //имя роли будет совпадать с иименем роли в методе hasRole("ADMIN")(в БД - ROLE_ADMIN)
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    //просрочился\не просрочился
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override//например добавить поле в бд к таблице пользователей,
    //а потом передавать это поле сюда дабы посмотреть активен ли юзер или нет
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    //просрочился\не просрочился
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @JsonIgnore
    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public Long getUserId() {
        return userid;
    }
    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    public List<RoleEntity> getRoles() {
        return roles;
    }

    public void setEnable(boolean enable){
        this.enabled = enable;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserId(Long id) {
        this.userid = id;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public void setRoles(RoleEntity role) {
        roles.add(role);
    }

    public void setRoleUser(){
        PrivilegeEntity privilege = new PrivilegeEntity();
        privilege.setName("READ");
        ArrayList<PrivilegeEntity> listPrivilege = new ArrayList<>();
        listPrivilege.add(privilege);
        RoleEntity role = new RoleEntity();
        role.setRoleid(1L);//id userа
        role.setName("ROLE_USER");
        role.setPrivelege(listPrivilege);
        System.out.println("SETROLEUSER " + role);
        roles.add(role);
    }
    private Collection<? extends GrantedAuthority>
    mapRolesToAuthorities(Collection<RoleEntity> role){
        return roles.stream().map(
                r -> new SimpleGrantedAuthority(r.getName()))
                .collect(Collectors.toList());
    }
}

