package ru.digital_league.core.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RolesprivilegesEntity {
    private Long roleid;
    private Long privilegeid;
}
