package ru.digital_league.core.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmploymentEntity {

    private Long employmentId;
    /**
     * реализация оптимистической блокировки
     */
    /*@NotNull*/
    private Integer version;

    private LocalDate startDt;

    private LocalDate endDt;

    private Long workTypeId;

    private String organizationName;

    private String organizationAddress;

    private String positionName;

    private Long personId;
}
