package ru.digital_league.core.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LocationsEntity {
    private Integer locationId;

    private String streetAddress;

    private String city;
}