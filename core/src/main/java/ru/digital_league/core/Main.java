package ru.digital_league.core;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.digital_league.core.config.SpringConfig;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "MyAPI", version = "1.0",
description = "Main"))
public class Main {
    public static void main(String[] args) throws Exception {
/*        thread(new HelloWorldProducer(), false);
       *//* thread(new HelloWorldConsumer(), false);*//*
        Thread.sleep(1000);*/
        SpringApplication.run(SpringConfig.class, args);
    }
/*
    public static void thread(Runnable runnable, boolean daemon) {
        Thread brokerThread = new Thread(runnable);
        brokerThread.setDaemon(daemon);
        brokerThread.start();
    }

    public static class HelloWorldProducer implements Runnable {// send message(publisher)
        public void run() {
            try {
                // Create a ConnectionFactory
                ActiveMQConnectionFactory connectionFactory =
                        new ActiveMQConnectionFactory("vm://localhost");
                //address gde nahoditcya borker
                // Create a Connection
                Connection connection = connectionFactory.createConnection();
                //tcp
                connection.start();

                // Create a Session
                //coedinenie c brokerom
                Session session =
                        connection.createSession(false,
                                Session.AUTO_ACKNOWLEDGE);

                // Create the destination (Topic or Queue)
                //V Brokere cozdaem ochered(chere imya ocheredi kto
                // ugodno cmogut perebracivati coobcheniya
                Destination destination = session.createQueue("TEST.FOO");

                // Create a MessageProducer from the Session to the Topic or Queue
                //object kotoriy generirue object coobcheniy(ne cherez que
                // ibo privyazka k session)
                MessageProducer producer = session.createProducer(destination);
                //mode, coobcheniya libo budut save, libo ne budut (ne
                // budet v nashem sluchae)
                producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

                // Create a messages
                //message create s sessii
                String text = "Hello world! From: " + Thread.currentThread().getName() + " : " + this.hashCode();
                TextMessage message = session.createTextMessage(text);

                // Tell the producer to send the message
                System.out.println("Sent message: " + message.hashCode() + " : " + Thread.currentThread().getName());
                //otsilaem
                producer.send(message);

                // Clean up
                session.close();
                connection.close();
            } catch (Exception e) {
                System.out.println("Caught: " + e);
                e.printStackTrace();
            }
        }
    }

    public static class HelloWorldConsumer implements Runnable, ExceptionListener {
        public void run() {
            try {
                //RECEIVER MESSAGES
                // Create a ConnectionFactory
                ActiveMQConnectionFactory connectionFactory =
                        new ActiveMQConnectionFactory("vm://localhost");
                //failover:(tcp://primary:61616,tcp://secondary:61616)?randomize=false"
                //"vm://localhost"
                // Create a Connection
                Connection connection = connectionFactory.createConnection();
                connection.start();

                connection.setExceptionListener(this);

                // Create a Session
                Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

                // Create the destination (Topic or Queue)
                //podklyuchemsya rovno k takoije ocheredi
                //quee create c brokera
                Destination destination = session.createQueue("TEST.FOO");

                // Create a MessageConsumer from the Session to the Topic or Queue
                //natravlivaem na ochered
                MessageConsumer consumer = session.createConsumer(destination);

                // Wait for a message
                //prinimaem
                //1000 - zaderjka
                Message message = consumer.receive(1000);

                //ecli text - vivodim
                if (message instanceof TextMessage) {
                    TextMessage textMessage = (TextMessage) message;
                    String text = textMessage.getText();
                    System.out.println("Received: " + text);
                } else {
                    System.out.println("Received: " + message);
                }

                consumer.close();
                session.close();
                connection.close();
            } catch (Exception e) {
                System.out.println("Caught: " + e);
                e.printStackTrace();
            }
        }

        public synchronized void onException(JMSException ex) {
            System.out.println("JMS Exception occured.  Shutting down client.");
        }*/
    }



/*
    agile scrum - metodologii razrabotki system
    microservice - protivostavlenie malenkih servicov monolity
    raznica - monolit sychestvuet cam po sebe
    v microservicah funkcii rabotayut coobcha
microservici mogut obchatcya cherez db - trydno otclejivat kto kakie
dannie menyaet
cnachalo poyavilic containeri(docker)
container mojet coderjat okrujenie (raznie OS dlya opredelennoi zadachi)
cnachalo chroot -> potom cls containeri -> docker(virtualnoe prostranstvo)
docker polzuetsya yadrom linuxa, ostalnoe mojet bit dostavleno
luchshei systemoi dlya serverov yadlyaetsa centos
na golom jeleze - nabor virtualnih mashin, vnutri - sestemi v kontainerah
nado obrabativat 100000 zaprosov v sec, a nado 200000, chtobi povishat-
processor(verticalnoe mashtobirovanie),

neskolko ekzemplyarov odnogo i togoje servera(gorizontalnoe mashtabirovanie)
        (cdelat mnogo dohlih serverov vmesto odnogo mochnogo)
vhodnaya tochka odna (programnii server) (mail.ru, a ne mail2.ru, mail 3.ru)->
balansirochiki
        API gateway - pryachem mnogo microservisov za
        odnoy tochkoy vhoda GET gog.ru() -> GET gog1.ru, POST gog2.ru, gog3.ru

dlya upravleniya 400 konteinerami docker - orkestraciya ->
openshift, mojet zapuskat microsirvice, orkestrator mojet pri nagrozke na
microservice cozdat eche odin ekzeplyar i balancirovat
microservice c pomochiyu kontainera legko zapustit, s pomochiyu
orkestratora legro ...

cvyazivanie moduley - obichnie programnie vizovi (klass.method())
RPC - camaya pervaya - kogda pitaemcya imitirovat programmnie vizovi
kogda 2 cervera, cervica, i nado vizdat funkciyu odnogo v drugom ->
obolochka i vizivaem v ramkah odnogo

rest - resttemplate - obrachenie k drugomu mikroservicy
nedostatok - medlennii(rabotaet cherez http)
zamena - jrpc (rabotaen na protokole http2->bistree)
kafka - credstvo loggirovaniya, no mojno ispolzovat kak i sredstvo dlya
        obcheniya mikroservisov
AMQP - credstvo assinhronnogo obcheniya ->
reactivngoe programmirovanie (ot clova reakciya, otpravil chot-> zabil
        i ne jdesh kogda pridet otvet, no kogda pridet -> obrabativaesh)

latency, ... (zaderzhka, propusknaya sposobnocti)
        ecli ect varik podojdat - reaktivnoe programmirovanie
laiki v youtube - laik ctavish -> gde to tam obrabativaet skachkami
odin microservice - laiki pokazivaet
vtoroy - laiki podschitivatet

api odno - realizaciya ne vajno na kakom yazike...

docker compose - vozmojnost obedenit containeri v
        odno cetevoe addressnoe prostranstvo
management - orkestraciya (server gde nastroiki)
service discovery - mikroservicy hotchet obratitcya k drugomu
mikroservicy (host, ochered, ne vajno) -> obrachaetcya k server discovery
upavchie servici ubiraem - fancic,
novii ekzeplyar servica v drugom meste - nado kak-to obratitcya
-> toje savoe delaet API Gateway, rabotaet horosho, no ne v slojnih sistemah
mikroservis padaet -> nado podnyat, no s novim adresom
        ribbon obrachaetcya k eureka dlya polucheniya statistiki
EUREKA(ecli upadet - vse mikroservici razuchatcya obchatcya)
        - service discovery(mojet oprashivat, tablica so statusami)
balancirovka(ect odna vhodnaya tochka),
klentskaya blansirovka(ribbon) -> klient vibiraet k kakomu servicu
on hochet obratittcya(ribbon obrahchaetcya k service discovery(spisok
        mikroservicov))

kod ne doljen znat gde on doljen ispolnyatcya

ZUUL - APi gateway
ngings cherez sebya securnii context propuskat ne budet
Haproxy

        push->jenkinsbuild(build container and push on nexus)
kubernetis razveni kak servici - razvorachivaet
kubernetis - raspredelennaya sistema, mojet rabotat na neskolkih serverah
kubernetis pod upravleniem

probracivat security context cherez api gateway
RAFT
DIZNBUG(bag kot clojno ponyat)

rest obrachaetsya po jrpc k drugomu servicy
        agile/scrum-
        razrabotky c davnih vremen rassmatrivali kak ochen clojnii process->
        co vremenem nad bistro->agile - podstraivatcya pod nujdi zakazchika
scrum -resprostranennaya, ectb ponyatie komanda,
productowner - prestavlyaet interesi zakazchikov, obchaetcya c zakazchikom
backlog - hraniliche zadach
cprint - vremennoi otrezok za kotorii vipuskatcya novaya verciya
1)vzyat iz backloga zadachu i podumat naskolko slojno*/
/*
hashicorp vault*/
