package ru.digital_league.api;

import ru.digital_league.domain.dto.EmploymentDTO;
import ru.digital_league.domain.dto.PersonDTO;

import java.util.List;

public interface PersonService {
    PersonDTO save(PersonDTO personDTO);
    List<PersonDTO> getListPerson();
}
