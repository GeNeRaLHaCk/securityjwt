package ru.digital_league.api;

import org.springframework.security.core.Authentication;
import ru.digital_league.domain.dto.AccountDTO;
import ru.digital_league.domain.dto.LogInDTO;
import ru.digital_league.domain.dto.RegisterDTO;

public interface UserAuthenticationService {

    Authentication authorize(LogInDTO loginDto) throws RuntimeException;
    AccountDTO registerUser(RegisterDTO registerDto) throws Exception;
    void logout();
    //AccountDTO findByToken(String token);
    //void logout(String token);
}
