package ru.digital_league.api;

import ru.digital_league.domain.dto.EmploymentDTO;

import java.util.List;

public interface EmploymentService {
    EmploymentDTO save(EmploymentDTO employmentDTO);
    List<EmploymentDTO> getListEmployments();
}
