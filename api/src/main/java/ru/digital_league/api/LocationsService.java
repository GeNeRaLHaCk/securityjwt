package ru.digital_league.api;

import ru.digital_league.domain.dto.LocationsDTO;

import java.util.List;

public interface LocationsService {
    LocationsDTO getLocationById(Integer id);

    LocationsDTO insertLocation(LocationsDTO dto);

    List<LocationsDTO> getAll();

}
