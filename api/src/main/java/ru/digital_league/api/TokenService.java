/*
package ru.digital_league.domain.api;

import lombok.Builder;
import lombok.Data;
<<<<<<< HEAD:api/src/main/java/ru/digital_league/api/TokenService.java
import ru.digital_league.core.model.RefreshToken;
import ru.digital_league.core.entity.UserEntity;
=======
import ru.digital_league.domain.model.RefreshToken;
import ru.digital_league.domain.entity.UserEntity;
>>>>>>> 810d52e5f3def3c9d502227a4e062cdf58d03c43:dto/src/main/java/ru/digital_league/domain/api/TokenService.java

public interface TokenService {
    @Data
    @Builder
    class UserContext {

        private String login;

        public static UserContext of(UserEntity user) {
            return UserContext.builder()
                    .login(user.getUsername())
                    .build();
        }
    }
    String getUsername(String token);

    String newAccessToken(UserContext ctx);

    boolean isValidAccessToken(String token);

    RefreshToken newRefreshToken(UserContext ctx);

    RefreshToken findRefreshToken(Long tokenId);

    boolean isValidRefreshToken(RefreshToken token);
}
*/
