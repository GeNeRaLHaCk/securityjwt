FROM openjdk:8
RUN apt-get -y update
RUN apt-get -y upgrade 
RUN apt-get install -y maven 
COPY pom.xml /usr/local/service/pom.xml
COPY dependency-bom  /usr/local/service/dependency-bom
COPY api /usr/local/service/api
COPY core  /usr/local/service/core
COPY dto  /usr/local/service/dto
COPY migration  /usr/local/service/migration
WORKDIR /usr/local/service
RUN mvn compile
RUN mvn clean install -U
RUN mvn package
EXPOSE 8000
CMD["mvn", "-Pdev", "liquibase:updateSQL"]
ENTRYPOINT ["java", "-jar", "core/target/core-1.0-SNAPSHOT.jar"]
#CMD ["java", "-jar", "~/.m2/repository/digital_league/core/1.0-SNAPSHOT/core-1.0-SNAPSHOT.jar"]
#CMD ["java", "-cp", "core/target/core-1.0-SNAPSHOT.jar", "ru.digital_league.core.Main"]

#FROM haproxy:1.7
#COPY haproxy/haproxy.cfg /usr/locale/etc/haproxy/haproxy.cfg
#FROM nginx:1.17.10
#COPY nginx.conf /etc/nginx/nginx.conf
