package ru.digital_league.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(title = "Модель данных локации")
public class LocationsDTO {
    @Schema(title = "Идентификатор локации")
    private Integer locationId;

    @Schema(title = "Уличный адрес")
    private String streetAddress;

    @Schema(title = "город", required = true)
    private String city;

}
