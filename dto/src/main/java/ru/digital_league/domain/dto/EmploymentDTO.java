package ru.digital_league.domain.dto;

import org.jetbrains.annotations.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmploymentDTO {

    private Long employmentId;
    /**
     * реализация оптимистической блокировки
     */
    @NotNull
    private Integer version;

    private LocalDate startDt;

    private LocalDate endDt;

    private Long workTypeId;

    private String organizationName;

    private String organizationAddress;

    private String positionName;

    private Long personId;

}
