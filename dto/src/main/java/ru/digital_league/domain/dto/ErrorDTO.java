package ru.digital_league.domain.dto;

import lombok.Data;

@Data
public class ErrorDTO extends Exception{
    public ErrorDTO(String message){
        super(message);
        this.message = message;
    }
    private String message;
}
