package ru.digital_league.domain.dto;

import lombok.Data;
import org.jetbrains.annotations.NotNull;

import java.util.Date;

@Data
public class PersonDTO {

    private Long personId;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    private Date birthDate;

    @NotNull
    private String gender;

}
