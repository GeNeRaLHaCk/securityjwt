package ru.digital_league.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class RoleDTO {
    public RoleDTO(){}
    private Long roleid;
    private String rolename;
    private List<PrivilegeDTO> privilege = new ArrayList<>();
}
