package ru.digital_league.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class PrivilegeDTO {
    public PrivilegeDTO(){}
    private Long privilegeid;
    private String privilegename;
    private List<RoleDTO> roles = new ArrayList<>();
}
