package ru.digital_league.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AccountDTO {
    public AccountDTO(){}
    private Long userid;
    private String username;
    private String password;
    private boolean enabled = false;
}
