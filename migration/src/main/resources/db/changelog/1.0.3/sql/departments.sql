create table departments
(
    department_id   serial      not null
        constraint departments_pkey
            primary key,
    department_name varchar(30) not null,
    manager_id      integer,
    location_id     integer
        constraint departments_location_id_fkey
            references locations
);