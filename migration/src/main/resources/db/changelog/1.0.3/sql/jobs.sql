create table jobs
(
    job_id     varchar     not null
        constraint job_pkey
            primary key,
    job_title  varchar(35) not null,
    min_salary integer,
    max_salary integer
);