create table locations
(
    location_id    serial      not null
        constraint locations_pkey
            primary key,
    street_address varchar(40),
    city           varchar(30) not null
);
