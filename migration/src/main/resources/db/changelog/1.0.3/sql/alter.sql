alter table departments
    add constraint departments_manager_id_fkey
        foreign key (manager_id) references employees;

create unique index employees_email_uindex
    on employees (email);