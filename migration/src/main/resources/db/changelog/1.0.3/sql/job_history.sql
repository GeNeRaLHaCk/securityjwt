create table job_history
(
    start_date    date        not null,
    end_date      date        not null,
    job_id        varchar(10) not null
        constraint job_history_job_id_fkey
            references jobs,
    department_id integer
        constraint job_history_department_id_fkey
            references departments,
    employee_id   integer     not null
        constraint job_history_employee_id_fkey
            references employees,
    constraint job_history_pkey
        primary key (start_date, employee_id)
);