INSERT INTO public.departments (department_id, department_name, manager_id, location_id) VALUES (1, 'Отдел маркетинга и рекламы', 8, 1);
INSERT INTO public.departments (department_id, department_name, manager_id, location_id) VALUES (2, 'Отдел разработки ПО', 9, 1);

INSERT INTO public.employees (employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, manager_id, department_id) VALUES (4, 'Всеволод', 'Максимов', 'maksimov@mail.ru', '89343546543', '2020-01-05', '4', 80000, 9, null);
INSERT INTO public.employees (employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, manager_id, department_id) VALUES (9, 'Павел', 'Козлов', 'kozlov@mail.ru', '89357475834', '2020-01-01', '8', 50000, 8, null);
INSERT INTO public.employees (employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, manager_id, department_id) VALUES (7, 'Кира', 'Коршунова', 'korshunova@mail.ru', '89352538435', '2020-01-08', '7', 50000, 8, null);
INSERT INTO public.employees (employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, manager_id, department_id) VALUES (6, 'Екатерина', 'Никифорова', 'nikiforova@mail.ru', '89353464535', '2020-01-07', '6', 90000, 9, null);
INSERT INTO public.employees (employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, manager_id, department_id) VALUES (5, 'Анна', 'Тихонова', 'tihonova@mail.ru', '89453564364', '2020-01-06', '5', 65000, 8, null);
INSERT INTO public.employees (employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, manager_id, department_id) VALUES (8, 'Дмитрий', 'Буров', 'burov@mail.ru', '89345635434', '2020-01-01', '8', 50000, 9, null);
INSERT INTO public.employees (employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, manager_id, department_id) VALUES (3, 'Марина', 'Лаврентьева', 'marina@mail.ru', '89385394953', '2020-01-05', '3', 60000, 8, null);
INSERT INTO public.employees (employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, manager_id, department_id) VALUES (1, 'Марк', 'Болдырев', 'mark@mail.ru', '89999473533', '2020-02-04', '1', 50000, 8, null);
INSERT INTO public.employees (employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, manager_id, department_id) VALUES (2, 'Савелий', 'Фролов', 'savelii@mail.ru', '89438384850', '2020-01-04', '2', 70000, 9, null);

INSERT INTO public.job_history (start_date, end_date, job_id, department_id, employee_id) VALUES ('2019-01-01', '2019-06-01', '1', 2, 1);
INSERT INTO public.job_history (start_date, end_date, job_id, department_id, employee_id) VALUES ('2019-02-01', '2019-07-01', '2', 2, 2);
INSERT INTO public.job_history (start_date, end_date, job_id, department_id, employee_id) VALUES ('2017-01-01', '2019-06-01', '3', 2, 3);
INSERT INTO public.job_history (start_date, end_date, job_id, department_id, employee_id) VALUES ('2019-03-01', '2019-09-01', '4', 2, 4);
INSERT INTO public.job_history (start_date, end_date, job_id, department_id, employee_id) VALUES ('2018-02-01', '2019-01-01', '5', 2, 5);
INSERT INTO public.job_history (start_date, end_date, job_id, department_id, employee_id) VALUES ('2016-02-01', '2019-07-01', '6', 2, 6);
INSERT INTO public.job_history (start_date, end_date, job_id, department_id, employee_id) VALUES ('2015-02-01', '2019-07-01', '1', 2, 7);
INSERT INTO public.job_history (start_date, end_date, job_id, department_id, employee_id) VALUES ('2019-04-01', '2019-12-01', '7', 1, 8);
INSERT INTO public.job_history (start_date, end_date, job_id, department_id, employee_id) VALUES ('2019-05-01', '2019-11-01', '8', 1, 9);

INSERT INTO public.jobs (job_id, job_title, min_salary, max_salary) VALUES ('1', 'Программист', 50000, 200000);
INSERT INTO public.jobs (job_id, job_title, min_salary, max_salary) VALUES ('2', 'Архитектор бизнес-процессов', 70000, 300000);
INSERT INTO public.jobs (job_id, job_title, min_salary, max_salary) VALUES ('3', 'Администратор баз данных', 60000, 150000);
INSERT INTO public.jobs (job_id, job_title, min_salary, max_salary) VALUES ('4', 'Администратор защиты', 80000, 180000);
INSERT INTO public.jobs (job_id, job_title, min_salary, max_salary) VALUES ('5', 'Аналитик программного обеспечения', 65000, 190000);
INSERT INTO public.jobs (job_id, job_title, min_salary, max_salary) VALUES ('6', 'Архитектор программного обеспечения', 90000, 250000);
INSERT INTO public.jobs (job_id, job_title, min_salary, max_salary) VALUES ('7', 'Таргетолог', 50000, 170000);
INSERT INTO public.jobs (job_id, job_title, min_salary, max_salary) VALUES ('8', 'HR-менеджер', 50000, 130000);

INSERT INTO public.locations (location_id, street_address, city) VALUES (1, 'Авангардная улица', 'Москва');
INSERT INTO public.locations (location_id, street_address, city) VALUES (2, 'Авангардная улица', 'Москва');
INSERT INTO public.locations (location_id, street_address, city) VALUES (3, 'Абрикосовский переулок', 'Москва');
INSERT INTO public.locations (location_id, street_address, city) VALUES (4, 'Авиаторов, улица', 'Москва');
INSERT INTO public.locations (location_id, street_address, city) VALUES (5, 'Андроньевский проезд', 'Москва');
INSERT INTO public.locations (location_id, street_address, city) VALUES (6, 'Аносова, улица', 'Москва');
