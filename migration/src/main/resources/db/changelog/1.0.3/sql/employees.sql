create table employees
(
    employee_id   serial      not null
        constraint employees_pkey
            primary key,
    first_name    varchar(20),
    last_name     varchar(25) not null,
    email         varchar(25) not null,
    phone_number  varchar(20),
    hire_date     date        not null,
    job_id        varchar(10) not null
        constraint employees_job_id_fkey
            references jobs,
    salary        integer,
    manager_id    integer
        constraint employees_manager_id_fkey
            references employees,
    department_id integer
        constraint employees_department_id_fkey
            references departments
);