create table rolesprivileges
(
    roleid      bigint not null
        constraint rolesprivileges_role_roleid_fk
            references role
            on delete cascade,
    privilegeid bigint not null
        constraint rolesprivileges_privilege_id_fk
            references privilege
            on delete cascade
);