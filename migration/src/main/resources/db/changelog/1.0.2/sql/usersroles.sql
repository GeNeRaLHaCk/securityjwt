create table usersroles
(
    userid bigint not null
        constraint usersroles_usertable_userid_fk
            references usertable
            on delete cascade,
    roleid bigint not null
        constraint usersroles_role_roleid_fk
            references role
            on delete cascade
);
INSERT INTO public.role (roleid, rolename) VALUES (1, 'ROLE_ADMIN');
INSERT INTO public.role (roleid, rolename) VALUES (2, 'ROLE_USER');
INSERT INTO public.privilege (privilegeid, privilegename) VALUES (1, 'READ');
INSERT INTO public.privilege (privilegeid, privilegename) VALUES (2, 'WRITE');
INSERT INTO public.privilege (privilegeid, privilegename) VALUES (3, 'EXECUTE');
INSERT INTO public.usertable (userid, password, username) VALUES (1, '$2a$10$lFid6jorM6uT8U/.Za1FB.5hV6AIZsk70xZ/oef49jYttSQ3PaCLG', 'admin');
INSERT INTO public.usertable (userid, password, username) VALUES (2, '$2a$10$q6TyPMx3P2TrHYbf9GaWIuHCDkN73rQKIW.TO9zybNv5CG.GV5fxK', 'user');
INSERT INTO public.usersroles (userid, roleid) VALUES (1, 1);
INSERT INTO public.usersroles (userid, roleid) VALUES (2, 2);
INSERT INTO public.rolesprivileges (roleid, privilegeid) VALUES (1, 1);
INSERT INTO public.rolesprivileges (roleid, privilegeid) VALUES (1, 2);
INSERT INTO public.rolesprivileges (roleid, privilegeid) VALUES (1, 3);
INSERT INTO public.rolesprivileges (roleid, privilegeid) VALUES (2, 1);
